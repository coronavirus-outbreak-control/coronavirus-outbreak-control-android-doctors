<img src="images/LogoDoc.svg" alt="Logo" height="120">
  

<!-- ABOUT THE PROJECT -->
# About CoviDoc

CoviDoc - Covid Community Alert is an open-source app that can be used by doctors change a patient's status in [CovidApp](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-android)

This app goes in pair with [CovidApp](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-android),
the Bluetooth-based contact tracing app.

## Features
The app allows doctors to:
* Change the patient status to `Positive`/`Negative to Coronavirus`/`Healed` by:
  * Scanning a patient's QR code that can be found on the patient's CovidApp
  * Insert manually a patient ID
* Invite other Doctors to the app


### Built With
* [Android Studio](https://developer.android.com/studio)
* [Java](https://www.java.com/en/download/)



<!-- GETTING STARTED -->
## Getting Started
Just clone the repo and open it with Android Studio!



<!-- ROADMAP -->
### Roadmap

See the [open Issues](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-android-doctors/-/issues) for a list of proposed features (and known issues).
If you find any issues we are not aware of in the app, please **open an issue**! We will really appreciate your contributions.


<!-- CONTRIBUTING -->
### Contributing

If you believe in this project and in the importance of privacy of the user, just like we do, please contribute. It's easy:

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request. We'll review it and merge it!



<!-- LICENSE -->
## License

Distributed under the Apache 2.0 License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact
Elena Martina - Android Lead - [LinkedIn](https://www.linkedin.com/in/elena-martina/) - elenamartina98@gmail.com

Coronavirus Outbreak Control - [Website](https://coronavirus-outbreak-control.github.io/web/index.html) - coronavirus.outbreak.control@gmail.com




