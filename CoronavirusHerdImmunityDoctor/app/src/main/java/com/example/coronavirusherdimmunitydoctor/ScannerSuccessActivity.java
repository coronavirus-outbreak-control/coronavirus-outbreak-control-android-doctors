package com.example.coronavirusherdimmunitydoctor;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.coronavirusherdimmunitydoctor.enums.PatientStatus;
import com.example.coronavirusherdimmunitydoctor.models.CovidDialog;
import com.example.coronavirusherdimmunitydoctor.models.TouchListener;
import com.example.coronavirusherdimmunitydoctor.utils.ApiManager;
import com.example.coronavirusherdimmunitydoctor.utils.PreferenceManager;

import org.json.JSONObject;

import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import okhttp3.Response;

public class ScannerSuccessActivity extends AppCompatActivity {

    private Button bt_cancel;
    private Button bt_yes;
    private Button bt_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner_success);

        TextView tv_formatted_id = findViewById(R.id.tv_formatted_id);
        bt_cancel = findViewById(R.id.bt_cancel);
        bt_yes = findViewById(R.id.bt_yes);
        bt_no = findViewById(R.id.bt_no);

        Intent intent = getIntent();
        final long patient_id_cs = intent.getLongExtra("patient id", -1); //get 'patient id' with checksum recognized by QR code scanner

        String formattedId = Long.toString(patient_id_cs);
        formattedId = formattedId.replaceAll("..", "$0-");

        tv_formatted_id.setText(formattedId);

        /****************** Yes Button *******************/
        TouchListener.buttonClickEffect(bt_yes);   //add click button effect
        bt_yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean ignore_status_check = true; //used to ignore previous patient status when new status is going to be 'Infected'

                Intent intent=new Intent(getApplicationContext(), ChangeStatusActivity.class);
                intent.putExtra("patient id", patient_id_cs);   // give 'patientId + checksum' to next activity in order to change status of patient
                intent.putExtra("ignore status check", ignore_status_check);   // give 'ignore_status_check = true' to next activity in order to ignore previous status of patient
                startActivity(intent);
            }
        });

        /****************** No Button *******************/
        TouchListener.buttonClickEffect(bt_no);   //add click button effect
        bt_no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                long pat_id = patient_id_cs/10;  // remove checksum by patient id
                task_updateUserStatus(pat_id, "SUSPECTED");
            }
        });

        /****************** Cancel Button *******************/
        TouchListener.buttonClickEffect(bt_cancel);   //add click button effect
        bt_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /******************** Task Functions *********************************************/


    /**
     * if Doctor does not know swab results then go to MainActivity and the Status is updated to 'Suspected'
     * Run task in order to call updateUserStatus API and manage the response
     *
     * @param pat_id:         patient id
     * @param str_new_status: health patient status (pending/suspect)
     */
    private void task_updateUserStatus(final long pat_id, final String str_new_status) {

        Task.callInBackground(new Callable<String>() {
            @Override
            public String call() throws Exception {

                Integer new_status = PatientStatus.valueOf(str_new_status).ordinal();     // get 'enum' value of PatientStatus

                PreferenceManager pm = new PreferenceManager(getApplicationContext());

                Boolean updated = false;
                String ret_value = "";

                while (updated == false) {

                    Response response_updateUS = ApiManager.updateUserStatus(pat_id, new_status, pm.getJwtToken(), false);  //call updateUserStatus

                    if (response_updateUS != null) {
                        switch (response_updateUS.code()) {//check response status(code)
                            case 200:     // if response is 'ok' -> status has been changed
                                Log.d("task_updateUserStatus", "status has been changed");
                                ret_value = "chg_st";
                                updated = true;
                                break;
                            case 403:     // if jwt token is not sent -> call refreshJwtToken and recall task_updateUserStatus
                            case 401:     // if jwt token is expired -> call refreshJwtToken and recall task_updateUserStatus
                                Log.d("task_updateUserStatus", "Jwt Token expired");

                                Response response_refreshJwtToken = ApiManager.refreshJwtToken(pm.getAuthorizationToken());  //call refreshJwtToken
                                if (response_refreshJwtToken != null &&
                                        response_refreshJwtToken.code() == 200) { //check response status(code)

                                    try {
                                        String strResponse_body = response_refreshJwtToken.body().string();  //get body of Response
                                        JSONObject response_body = new JSONObject(strResponse_body);
                                        pm.setJwtToken(response_body.getString("token"));              //save new Jwt Token in shared preferences

                                    } catch (Exception e) {
                                        Log.d("task_updateUserStatus", "Exception to read jwt token received: " + e);
                                    }
                                }
                                break;
                            case 429: //rate limiter reached up in 1 minute, doctor has to wait to call this api
                                Log.d("task_updateUserStatus", "rate limiter reached up in 1 minute");
                                ret_value = "ratelim";
                                updated = true;
                                break;
                            default:
                                Log.d("task_updateUserStatus", "Code not recognized:" + response_updateUS.code());
                                ret_value = "not_rec";
                                updated = true;
                                break;
                        }
                    } else {  // no response from Backend (like: internet disabled)
                        Log.d("task_updateUserStatus", "No response by updateUserStatus");
                        ret_value = "no_resp";
                        updated = true;

                    }
                }

                return ret_value;
            }
        }).onSuccess(new Continuation<String, Object>() {
            @Override
            public String then(Task<String> task) throws Exception {

                switch (task.getResult()) {
                    case "chg_st":  //healths status changed -> go to next activity

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        break;
                    case "not_rec": //patient id not recognized
                        Toast.makeText(getApplicationContext(), R.string.toast_scanner_code_not_recognized, Toast.LENGTH_LONG).show();
                        break;
                    case "no_resp": // no response from Backend (like: internet disabled)
                        Toast.makeText(getApplicationContext(), R.string.toast_scanner_no_resp_change_status, Toast.LENGTH_LONG).show();
                        break;
                    case "ratelim": //rate limiter reached up in 1 minute, doctor has to wait to call this api
                        CovidDialog.errorDialog(ScannerSuccessActivity.this,
                                                        getString(R.string.aldiag_err_updatestatus_title),
                                                        getString(R.string.aldiag_ratelimiter_updatestatus)); //display error dial
                    default: //some errors
                        Toast.makeText(getApplicationContext(), R.string.toast_scanner_err_status_change, Toast.LENGTH_LONG).show();
                        break;
                }

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

}
