package com.example.coronavirusherdimmunitydoctor.invitecontacts;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.example.coronavirusherdimmunitydoctor.R;
import com.example.coronavirusherdimmunitydoctor.models.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ListContactsAdapter extends RecyclerView.Adapter<ListContactsAdapter.ViewHolder> {

    private final Map<Contact,ArrayList<String>> mapContacts;
    private final Set<Contact> keySet;
    private final ArrayList<Contact> keyList;

    private final RequestManager glide;
    private final ListContactsAdapter.OnContactClick onContactClickListener;

    private Context mContext;

    public ListContactsAdapter(Context ctx, HashMap<Contact,ArrayList<String>> map, RequestManager glide, ListContactsAdapter.OnContactClick onContactClickListener) {
        this.mContext = ctx;
        this.mapContacts = map;
        keySet = map.keySet();
        keyList = new ArrayList<Contact>(keySet);
        this.glide = glide;
        this.onContactClickListener = onContactClickListener;
    }

    @NonNull
    @Override
    public ListContactsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_list, parent, false);
        return new ListContactsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListContactsAdapter.ViewHolder holder, int position) {
        Contact key = keyList.get(position);
        holder.bind(key, mapContacts.get(key));
    }

    @Override
    public int getItemCount() {
        return mapContacts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView nameTextView;
        final Spinner phoneList;
        final TextView photoTextView;
        final ImageView photoImageView;
        final ImageView checkedImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.name);
            phoneList = itemView.findViewById(R.id.phone);
            photoImageView = itemView.findViewById(R.id.photo);
            photoTextView = itemView.findViewById(R.id.photo_text);
            checkedImageView = itemView.findViewById(R.id.checked);
        }

        public void bind(final Contact contact, final ArrayList<String> phones) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String selected_phone = phoneList.getSelectedItem().toString();
                    contact.setPhone(selected_phone);
                    onContactClickListener.onContactClicked(contact);
                    bind(contact, phones);
                }
            });
            nameTextView.setText(contact.getName());
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.support_simple_spinner_dropdown_item, phones);
            phoneList.setAdapter(adapter);

            final String photo = contact.getPhoto();
            if (photo != null) {
                photoImageView.setVisibility(View.VISIBLE);
                photoTextView.setVisibility(View.GONE);
                glide.load(photo).centerCrop().into(photoImageView);
            } else {
                photoImageView.setVisibility(View.INVISIBLE);
                photoTextView.setVisibility(View.VISIBLE);
                photoTextView.setText(contact.getFirstLetter());
            }

            if (contact.isSelected()) {
                checkedImageView.setVisibility(View.VISIBLE);
            } else {
                checkedImageView.setVisibility(View.INVISIBLE);
            }
        }
    }

    interface OnContactClick {
        void onContactClicked(Contact contact);
    }
}
