package com.example.coronavirusherdimmunitydoctor.introduction;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.example.coronavirusherdimmunitydoctor.R;

import com.example.coronavirusherdimmunitydoctor.HowItWorksActivity;
import com.example.coronavirusherdimmunitydoctor.models.TouchListener;
import com.example.coronavirusherdimmunitydoctor.utils.Security;


public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        setContentView(R.layout.intro0_welcome);


        this.writeTitle();


        Button start_button = (Button) findViewById(R.id.button_next);
        TouchListener.buttonClickEffect(start_button);   //add click button effect
        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, CameraPermissionActivity.class));
            }
        });

        Button how_it_works_button = (Button) findViewById(R.id.how_it_works);
        TouchListener.buttonClickEffect(how_it_works_button);   //add click button effect
        how_it_works_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, HowItWorksActivity.class));
            }
        });

    }

    private void writeTitle() {

        String first_line = getResources().getString(R.string.welcome_first);
        String second_line = getResources().getString(R.string.welcome_second);
        String third_line = getResources().getString(R.string.welcome_third);
        String blue = getResources().getString(R.string.welcome_blue);
        String last = getResources().getString(R.string.welcome_last);
        TextView t = (TextView) findViewById(R.id.welcome_to);

        t.setText(Html.fromHtml(first_line +
                "<br/><br/>" + second_line +
                "<br/>" + third_line +
                "<br/><font color='#16ACEA'>" +
                blue + "</font><br/>" + last));
    }

}